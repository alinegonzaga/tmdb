package br.com.challenge.network;

import java.util.List;

import br.com.challenge.network.response.Movie;

public interface OnGetMoviesCallback {
    void onSuccess(List<Movie> movies);

    void onError();
}

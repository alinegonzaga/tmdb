package br.com.challenge.ui.filmes;

import java.util.List;
import br.com.challenge.network.MoviesRepository;
import br.com.challenge.network.OnGetMoviesCallback;
import br.com.challenge.network.response.Movie;

public class ListaSearchPresenter implements ListaFilmesContrato.SearchListPresenter {

    private ListaFilmesContrato.ListaFilmesView view;
    private MoviesRepository moviesRepository;

    public ListaSearchPresenter (ListaFilmesContrato.ListaFilmesView view) {
        this.view = view;
    }

    @Override
    public void obtemFilmes(String search) {
        moviesRepository = MoviesRepository.getInstance();

        moviesRepository.getSearch(new OnGetMoviesCallback() {
            @Override
            public void onSuccess(List<Movie> movies) {
                view.mostraFilmes(movies);
            }

            @Override
            public void onError() {
                view.mostraErro();
            }
        }, search);
    }

    @Override
    public void destruirView() {
        this.view = null;
    }
}

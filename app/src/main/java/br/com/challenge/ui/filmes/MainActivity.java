package br.com.challenge.ui.filmes;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import br.com.challenge.R;
import br.com.challenge.ui.filmes.listafilmes.ActionFragment;
import br.com.challenge.ui.filmes.listafilmes.DramaFragment;
import br.com.challenge.ui.filmes.listafilmes.FantasyFragment;
import br.com.challenge.ui.filmes.listafilmes.FictionSciencsFragment;
import br.com.challenge.ui.filmes.pesquisa.SearchActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle("Movies");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TabLayout mtabLayout = (TabLayout) findViewById(R.id.tab_layout);
        ViewPager mViewPager = (ViewPager) findViewById(R.id.view_pager);

        FragmantPag customPagerAdapter = new FragmantPag(getSupportFragmentManager(), this);
        customPagerAdapter.add(new ActionFragment(), "Ação");
        customPagerAdapter.add(new DramaFragment(), "Drama");
        customPagerAdapter.add(new FantasyFragment(), "Fantasia");
        customPagerAdapter.add(new FictionSciencsFragment(), "Ficção");

        mViewPager.setAdapter(customPagerAdapter);
        mtabLayout.setupWithViewPager(mViewPager);
        mViewPager.setOffscreenPageLimit(3);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search:
                 startActivity(new Intent(MainActivity.this, SearchActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
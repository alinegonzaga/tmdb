package br.com.challenge.ui.filmes.listafilmes;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import br.com.challenge.ui.filmes.ItemClickListener;
import br.com.challenge.ui.filmes.ListaFilmesContrato;
import br.com.challenge.ui.filmes.ListaFilmesPresenter;
import br.com.challenge.ui.filmes.MoviesAdapter;
import br.com.challenge.ui.filmes.detalhesfilmes.DetailMovieActivity;
import br.com.challenge.network.response.Movie;

import br.com.challenge.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ActionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ActionFragment extends Fragment implements ItemClickListener, ListaFilmesContrato.ListaFilmesView {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private RecyclerView moviesList;
    private MoviesAdapter adapter;

    ListaFilmesContrato.ListafilmesPresenter presenter;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public ActionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ActionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ActionFragment newInstance(String param1, String param2) {
        ActionFragment fragment = new ActionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_action, container, false);

        moviesList = view.findViewById(R.id.moviesList);

        presenter = new ListaFilmesPresenter(this);
        presenter.obtemFilmes(28);

        return view;
    }

    @Override
    public void onClick(View view, int position) {
        final Movie movie = adapter.getItem(position);
        Intent intent = new Intent(getActivity(), DetailMovieActivity.class);
        intent.putExtra("pathImage", movie.getPosterPath());
        intent.putExtra("overview", movie.getOverview());
        intent.putExtra("title", movie.getTitle());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    @Override
    public void mostraFilmes(List<Movie> filmes) {
        adapter = new MoviesAdapter(getContext(), filmes);
        moviesList.setLayoutManager(new GridLayoutManager(getContext(), 2));
        moviesList.setAdapter(adapter);
        adapter.setClickListener(ActionFragment.this);
    }

    @Override
    public void mostraErro() {
        Toast.makeText(getActivity(), "Sem conexão.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.destruirView();
    }
}
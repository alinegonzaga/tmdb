package br.com.challenge.ui.filmes.detalhesfilmes;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import br.com.challenge.R;

public class DetailMovieActivity extends AppCompatActivity {

    private ImageView img;
    private TextView tvOverView;
    private String IMAGE_BASE_URL = "http://image.tmdb.org/t/p/w100";
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_movie);

        img = findViewById(R.id.img);
        tvOverView = findViewById(R.id.tvOverView);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
          String path =  bundle.getString("pathImage");
          String overview = bundle.getString("overview");
          String title = bundle.getString("title");

            setTitle(title);
            tvOverView.setText(overview);

            Glide.with(img)
                    .load(IMAGE_BASE_URL + path)
                    .apply(RequestOptions.placeholderOf(R.color.colorPrimary))
                    .into(img);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
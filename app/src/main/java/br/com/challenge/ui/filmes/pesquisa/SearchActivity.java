package br.com.challenge.ui.filmes.pesquisa;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;
import java.lang.reflect.Field;
import java.util.List;
import br.com.challenge.ui.filmes.ItemClickListener;
import br.com.challenge.ui.filmes.ListaFilmesContrato;
import br.com.challenge.ui.filmes.ListaSearchPresenter;
import br.com.challenge.ui.filmes.MoviesAdapter;
import br.com.challenge.ui.filmes.detalhesfilmes.DetailMovieActivity;
import br.com.challenge.network.response.Movie;
import br.com.challenge.R;

public class SearchActivity extends AppCompatActivity implements ItemClickListener, ListaFilmesContrato.ListaFilmesView {

    public MenuItem seachItem;
    private SearchView editsearch;
    private RecyclerView moviesList;
    private MoviesAdapter adapter;
    Context context = this;
    ActionBar actionBar;
    ListaFilmesContrato.SearchListPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        moviesList = findViewById(R.id.moviesList);

        actionBar = getActionBar();

        presenter = new ListaSearchPresenter(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_search, menu);
        seachItem = menu.findItem(R.id.search);
        editsearch = (SearchView) MenuItemCompat.getActionView(seachItem);

        editsearch.setFocusable(true);
        editsearch.setIconified(false);
        editsearch.requestFocusFromTouch();

        try {
            AutoCompleteTextView searchTextView = (AutoCompleteTextView) editsearch.findViewById(android.support.v7.appcompat.R.id.search_src_text);
            editsearch.setInputType(InputType.TYPE_CLASS_TEXT);
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
             mCursorDrawableRes.set(searchTextView, R.drawable.cursor);

        } catch (Exception e) {

        }

        editsearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (!newText.trim().equalsIgnoreCase("")) {
                    presenter.obtemFilmes(newText);
                }
                return false;
            }
        });

        return true;
    }

    @Override
    public void onClick(View view, int position) {
        final Movie movie = adapter.getItem(position);
        Intent intent = new Intent(SearchActivity.this, DetailMovieActivity.class);
        intent.putExtra("pathImage", movie.getPosterPath());
        intent.putExtra("overview", movie.getOverview());
        intent.putExtra("title", movie.getTitle());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void mostraFilmes(List<Movie> filmes) {
        adapter = new MoviesAdapter(context, filmes);
        moviesList.setLayoutManager(new GridLayoutManager(context, 2));
        moviesList.setAdapter(adapter);
        adapter.setClickListener(SearchActivity.this);
    }

    @Override
    public void mostraErro() {
        Toast.makeText(SearchActivity.this, "Sem conexão.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.destruirView();
    }
}
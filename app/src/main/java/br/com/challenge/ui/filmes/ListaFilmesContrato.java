package br.com.challenge.ui.filmes;

import br.com.challenge.network.response.Movie;
import java.util.List;

public interface ListaFilmesContrato {

    interface ListaFilmesView {
        void mostraFilmes(List<Movie> filmes);
        void mostraErro();
    }

    interface ListafilmesPresenter {
        void obtemFilmes(int genreType);
        void destruirView();
    }

    interface SearchListPresenter {
        void obtemFilmes(String search);
        void destruirView();
    }
}
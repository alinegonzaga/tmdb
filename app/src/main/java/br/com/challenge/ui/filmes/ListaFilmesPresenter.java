package br.com.challenge.ui.filmes;

import java.util.List;
import br.com.challenge.network.MoviesRepository;
import br.com.challenge.network.OnGetMoviesCallback;
import br.com.challenge.network.response.Movie;

public class ListaFilmesPresenter implements ListaFilmesContrato.ListafilmesPresenter{

    private ListaFilmesContrato.ListaFilmesView view;
    private MoviesRepository moviesRepository;

    public ListaFilmesPresenter (ListaFilmesContrato.ListaFilmesView view) {
        this.view = view;
    }

    @Override
    public void obtemFilmes(int genreType) {
        moviesRepository = MoviesRepository.getInstance();

        moviesRepository.getMovies(new OnGetMoviesCallback() {
            @Override
            public void onSuccess(List<Movie> movies) {
               view.mostraFilmes(movies);
            }

            @Override
            public void onError() {
                view.mostraErro();
            }
        }, genreType);
    }

    @Override
    public void destruirView() {
        this.view = null;
    }
}
